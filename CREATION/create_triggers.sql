/*
    DESCRIPTION TRIGGER a_insert_contratAA :
    S'assure dès la signature d'un contrat de l'agence avec un artiste, que ce dernier est representé par un de leurs agents
    pendant une durée d'un mois à compté de la signature du contrat en lui en affectant un.
*/

DROP FUNCTION insert_contratAA();
CREATE OR REPLACE FUNCTION insert_contratAA() RETURNS trigger AS $$
DECLARE
    id INTEGER;
    date_f DATE;

BEGIN
        id := (SELECT id_agent FROM agent ORDER BY random() lIMIT 1);
    date_f := (SELECT NEW.date_deb + INTERVAL '1 month');
    INSERT INTO represente(id_agent, id_artiste, date_deb, date_fin) VALUES (id, NEW.id_artiste, NEW.date_deb, date_f);
    RETURN NEW;
END;
$$ LANGUAGE plpgsql; 

CREATE TRIGGER a_insert_contratAA
AFTER INSERT ON contratAA 
FOR EACH ROW
EXECUTE PROCEDURE insert_contratAA();

/*
TEST qui prouve qu'à la creation d'un contrat avec un artiste (ici 501), un agent lui est assigné pour un mois dans la table represente.
    INSERT INTO client(nom,prenom,email,naissance) VALUES('Soukeina', 'Hadrami', 'souk@gm',DATE'2000-11-29');
    INSERT INTO artiste(id_artiste) VALUES(501);
    INSERT INTO contratAA(id_artiste,pourcentage,date_deb,date_fin) VALUES(501,0.1,'2017-04-07','2017-07-06');
    SELECT * FROM represente WHERE id_artiste = 501;
*/

 ----------------------------------------------------------------------------------------------------------------------------------------------

/*
    DESCRIPTION TRIGGER b_insert_represente :
    Empêche l'insertion d'un tuple qui assigne un agent à un artiste alors que ce dernier est dèja représenté par un autre agent à cette date 
    (empêche le chevauchement de dates de representation puisqu'un artiste ne peut être representé que par un agent au même temps).
*/

DROP FUNCTION representation();
CREATE OR REPLACE FUNCTION representation() RETURNS trigger AS $$
DECLARE
    max_date_fin DATE;

BEGIN  
    max_date_fin := (SELECT date_fin FROM represente WHERE id_artiste = NEW.id_artiste ORDER BY date_fin DESC LIMIT 1);
    
    IF max_date_fin IS NULL THEN 
        RETURN NEW; 
    ELSE IF NEW.date_deb > max_date_fin THEN 
        RETURN NEW;
    ELSE
        RAISE 'Un agent represente dèja l''artiste %', NEW.id_artiste || ' à cette periode. ' USING ERRCODE = 'unique_violation';
    END IF;  
    END IF;
END;
$$ LANGUAGE plpgsql; 

CREATE TRIGGER b_insert_represente
BEFORE INSERT ON represente  
FOR EACH ROW
EXECUTE PROCEDURE representation();

/*
TEST :
    SELECT * FROM represente WHERE id_artiste = 1;
    INSERT INTO represente(id_agent,id_artiste,date_deb,date_fin) VALUES(150,1,DATE'2019-02-02',DATE'2022-08-19');
    SELECT * FROM represente WHERE id_artiste = 1;
*/

 ----------------------------------------------------------------------------------------------------------------------------------------------

/*
DESCRIPTION TRIGGER  representation_periode :
    S'assure avant l'insertion d'un tuple dans la table represente que la période de
    representation est comprise dans la periode du contrat de l'artiste avec l'agence 
    sinon renvoie un message d'erreur.
*/

DROP FUNCTION representation_periode();
CREATE OR REPLACE FUNCTION representation_periode() RETURNS trigger AS $$
DECLARE
    debut_contrat DATE;
    fin_contrat   DATE;
    test BOOLEAN;

BEGIN
    debut_contrat := (SELECT date_deb FROM contratAA WHERE id_artiste = NEW.id_artiste);
    fin_contrat   := (SELECT date_fin FROM contratAA WHERE id_artiste = NEW.id_artiste);  

    IF (NEW.date_deb BETWEEN debut_contrat AND fin_contrat) AND (NEW.date_fin BETWEEN debut_contrat AND fin_contrat)
        THEN RETURN NEW;
    ELSE
        RAISE 'Un agent ne peut pas representer l''artiste %', NEW.id_artiste || ' pour cette période car elle dépasse sa période de contrat.' ;
    END IF;
END;
$$ LANGUAGE plpgsql; 

CREATE TRIGGER c_representation_periode
BEFORE INSERT OR UPDATE ON represente 
FOR EACH ROW
EXECUTE PROCEDURE representation_periode();

/*
 TEST :
    INSERT INTO represente(id_agent,id_artiste,date_deb,date_fin) VALUES(157,19,DATE'2023-12-26',DATE'2024-07-18');
*/

 ----------------------------------------------------------------------------------------------------------------------------------------------

/*
DESCRIPTION TRIGGER d_trig_audition :
    Set l'attribut retenu de la table proposition à TRUE si le profil recherché pour la demande correspond à une des formations suivies par l'artiste proposé.
*/

DROP FUNCTION retenu();
CREATE OR REPLACE FUNCTION retenu() RETURNS TRIGGER AS $$
DECLARE 
    prof INTEGER;

BEGIN 
    prof := (SELECT profil FROM demande WHERE id_demande = NEW.id_demande);
    
    IF prof IN (SELECT id_formation FROM formationArtiste WHERE id_artiste = NEW.id_artiste) THEN
        NEW.retenu = true ; 
    END IF;
    RETURN NEW;
END;
$$LANGUAGE plpgsql;

CREATE TRIGGER d_trig_audition
BEFORE INSERT ON proposition
FOR EACH ROW
EXECUTE PROCEDURE retenu();

/* TEST 
    INSERT INTO proposition(id_artiste,id_demande) VALUES (28,1); -- attribut retenu SET à FALSE par default est mis à TRUE lors de l'insertion;
    SELECT * FROM proposition WHERE id_artiste = 28 AND id_demande = 1;
*/

 ----------------------------------------------------------------------------------------------------------------------------------------------

/* DESCRIPTION TRIGGER e_limit_participation :
    Empêche un artiste de participer plus d'une fois à une même réalisation avec le même rôle. */

DROP FUNCTION participation_artiste();
CREATE OR REPLACE FUNCTION participation_artiste() RETURNS TRIGGER AS $$
DECLARE
    id_form INTEGER;
    i INTEGER;
    form VARCHAR(20);

BEGIN 
    SELECT id_formation INTO id_form FROM participation WHERE id_artiste = NEW.id_artiste AND id_real = NEW.id_real;
    form := (SELECT titre FROM formation WHERE id_formation = NEW.id_formation );

    IF id_form IS NULL
        THEN RETURN NEW;   
    ELSE  
        IF  NEW.id_formation NOT IN (SELECT id_formation FROM participation WHERE id_artiste = NEW.id_artiste AND id_real = NEW.id_real)
            THEN RETURN NEW;  
        ELSE
            RAISE 'L''artiste %', NEW.id_artiste || ' participe déjà à la réalisation ' || NEW.id_real|| ' en tant que ' || form ;
        END IF;          
    END IF;       
    RETURN NULL;      
END;
$$LANGUAGE plpgsql;

CREATE TRIGGER e_limit_participation
BEFORE INSERT ON participation
FOR EACH ROW
EXECUTE PROCEDURE participation_artiste();

/* TESTS TRIGGER e_limit_participation :

TEST du trigger en ajoutant un artiste dans participation avec une même formation pour une realisaion donnée une deuxième fois : 
    SELECT * FROM participation WHERE id_artiste = 107;
    INSERT INTO participation(id_artiste,id_real, id_formation) VALUES(107,44,3);
    SELECT * FROM participation WHERE id_artiste = 107;
*/

 ----------------------------------------------------------------------------------------------------------------------------------------------

/*
DESCRIPTION TRIGGER f_popularite :
    Augmente la popularité d'un artiste de dés qu'il participe à 5 nouvelles réalisations primées.
*/

CREATE OR REPLACE FUNCTION aug_popularite() RETURNS trigger AS $$
DECLARE 
    nb_participations INTEGER;
    artiste INTEGER;
    partmod INTEGER;
    pop INTEGER;

 BEGIN
    nb_participations := (SELECT count(*) FROM participation p JOIN realisation r ON p.id_real = r.id_real
                          WHERE id_artiste = NEW.id_artiste AND recompense <> 'NONE');

        pop := (SELECT popularite FROM artiste WHERE id_artiste = NEW.id_artiste); 
    partmod := (SELECT MOD(nb_participations,5)) ;

    IF partmod = 0  AND pop < 4 THEN
    UPDATE artiste SET popularite = pop + 1 where id_artiste = NEW.id_artiste ;
        RETURN NEW;
    ELSE IF(pop = 4)
        THEN RAISE 'Cet artiste est déja très populaire' USING ERRCODE = 'popularite >= 4';
    ELSE 
        RETURN NULL;     
    END IF;
    END IF;
END;
$$ LANGUAGE plpgsql; 

CREATE TRIGGER f_popularite
AFTER INSERT ON participation FOR EACH ROW
EXECUTE PROCEDURE aug_popularite();

/* TESTS TRIGGER f_popularite
 SELECT popularite FROM artiste WHERE id_artiste = 107;
 INSERT INTO participation(id_artiste,id_real, id_formation) VALUES(107,12,1);
 SELECT popularite FROM artiste WHERE id_artiste = 107;
*/

 ----------------------------------------------------------------------------------------------------------------------------------------------

/*
DESCRIPTION TRIGGER representation_periode :
   Pour chaque demande pour laquelle un artiste proposé à été retenu (attribut retenu de la table proposition a TRUE), un contratAP entre cet artiste 
   et le producteur est créé pour la période du projet. Il s'assure avant de créer ce contrat que l'artiste choisi est bien representé par un agent pour la
   période de la demande.Après la signature d'un contrat pour une certaine demande, l'attribut complet de la table demande est mis à TRUE ce qui 
   empêche d'autres artistes d'être proposé pour pour cette demande.
   Il défini également la participation (un pourcentage) qu'a l'artiste dans les bénéfices en fonction de sa popularité.
*/

DROP FUNCTION choix_artiste();
CREATE OR REPLACE function choix_artiste() RETURNS TRIGGER  AS $$
DECLARE 
    nb_ret INTEGER;
    comp BOOLEAN;
    pop INTEGER;
    remuneration INTEGER;
    pourcentage FLOAT;

    date_deb_rep DATE;
    date_fin_rep DATE;
    date_deb_demande DATE;
    date_fin_demande DATE;
    benefice INTEGER;
    
BEGIN   
    SELECT complet INTO comp FROM demande WHERE id_demande = NEW.id_demande;
    IF comp = FALSE THEN 
        -- On s'assure que l'artiste a un agent courant  
        date_deb_rep :=     (SELECT date_deb FROM represente WHERE id_artiste = NEW.id_artiste order by date_deb desc limit 1);
        date_fin_rep :=     (SELECT date_fin FROM represente WHERE id_artiste = NEW.id_artiste order by date_fin desc limit 1);
        date_deb_demande := (SELECT date_deb FROM demande WHERE id_demande = NEW.id_demande);
        date_fin_demande := (SELECT date_fin FROM demande WHERE id_demande = NEW.id_demande);

        -- On s'assure que la période de représentation de l'artiste par l'agent couvre
        -- bien celle la demande et qu'il a été retenu pour le projet de la demande.  
        IF(date_deb_demande >= date_deb_rep AND date_fin_demande <= date_fin_rep AND NEW.retenu = true) THEN
            remuneration := (SELECT random()*(4500-1500)+1500);
            pourcentage = 0;   
            SELECT INTO pop popularite FROM artiste WHERE id_artiste = NEW.id_artiste;
            CASE pop
                    WHEN 1 THEN
                        pourcentage = 0.05;
                    WHEN 2 THEN
                        pourcentage = 0.07;
                    WHEN 3 THEN
                        pourcentage = 0.1;
                    WHEN 4 THEN
                        pourcentage = 0.15;
                    WHEN 5 THEN
                        pourcentage = 0.2;
                    ELSE pourcentage = 0 ;
            END CASE;
            benefice := (SELECT random()*(2000-1500)+1500);          
            INSERT INTO contratAP (id_demande, id_artiste, remuneration, pourcentage,benefice) VALUES (NEW.id_demande, NEW.id_artiste,remuneration, pourcentage,benefice);
            UPDATE demande SET complet = TRUE WHERE id_demande = NEW.id_demande; 
            RETURN NEW;  
        ELSE 
            RETURN NEW;
        END IF;  
    END IF;   
    --RAISE NOTICE 'Impossible de proposer un nouvel artiste pour la demande %', NEW.id_demande ||' car un contrat a deja été signé pour cette dernière.' ;                   
    RETURN NULL;  
END;

$$ LANGUAGE plpgsql; 

CREATE TRIGGER g_choix_artiste
BEFORE INSERT ON proposition FOR EACH ROW
EXECUTE PROCEDURE choix_artiste();

/*
TEST TRIGGER g_choix_artiste:

    SELECT count(*) FROM proposition WHERE retenu = true;
    SELECT count(*) FROM demande WHERE complet = 1;
    SELECT count(*) FROM contratAP;
--------
    SELECT count(*) FROM contratAP;  -- Verifie que des tuples ont été inserés dans contratAP suite à une proposition retenue pour une demande.
    SELECT id_demande FROM demande WHERE complet = TRUE LIMIT 1; -- Query pour choisir une demande déja complète (ici la 44).
    INSERT INTO proposition(id_artiste,id_demande) VALUES (28,44); -- On s'est assurée que l'artiste 28 a suivi une formation qui correspond au profil de la demande.
*/

 ----------------------------------------------------------------------------------------------------------------------------------------------

/*
DESCRIPTION TRIGGER h_retard_paiement :
    Ce trigger est déclenché lorsqu'un paiement est effectué en retard par rapport à la date de versement prévue dans la table versement pour
    un certain contrat.Dans le cas d'un retard, le bénefice de ce contrat baisse de 5% pourcents et la difference est ajoutée
    à la remunération de l'artiste.
*/

DROP FUNCTION retard_paiement();
CREATE OR REPLACE FUNCTION retard_paiement() RETURNS trigger AS $$
DECLARE 
    date_prevu DATE;
    mont INTEGER;
    bnfice INTEGER;
BEGIN
    date_prevu := (SELECT date_prevue_vers FROM versement WHERE id_versement= NEW.id_versement AND id_contrat = NEW.id_contrat );
     
    IF NEW.date_paie > date_prevu THEN
        RAISE notice  'Le versement %', NEW.id_versement || ' pour le contrat '|| NEW.id_contrat || ' est en retard de ' || NEW.date_paie - date_prevu || ' jours.' ;
        
        bnfice := (SELECT benefice FROM contratAP WHERE id_contrat = NEW.id_contrat);
    
        UPDATE contratAP 
        SET remuneration = remuneration + benefice * 0.05 , benefice = benefice - benefice * 0.05
        WHERE id_contrat = NEW.id_contrat;
        
        UPDATE paiement 
        SET montant = NEW.montant + bnfice * 0.05 
        WHERE id_versement= NEW.id_versement AND id_contrat = NEW.id_contrat ; 

        RETURN NEW;
    END IF;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql; 


CREATE TRIGGER h_retard_paiement
AFTER INSERT ON paiement FOR EACH ROW
EXECUTE PROCEDURE retard_paiement();

/* TESTS TRIGGER h_retard_paiement
 TEST Montat remuneration et benefice avant retard du paiement :

 SELECT remuneration, benefice from contratAP where id_contrat = 1;
 INSERT INTO paiement(id_versement, id_contrat, date_paie, montant) VALUES (4,1,'2022-02-16',78);
 
 TEST Montat remineration et benefice apres retard du paiement :
 SELECT remuneration, benefice from contratAP where id_contrat = 1;
 SELECT * FROM paiement WHERE id_contrat = 1 AND id_versement = 4;
*/

 ----------------------------------------------------------------------------------------------------------------------------------------------

/*
    DESCRIPTION TRIGGER i_suppression_versement :
    Autorise la supression d'un versement prevu i pour un contrat k dans la table versement ssi le paiement effectué dans la table paiement
    correspond bien à la somme dûe. 
*/
CREATE OR REPLACE FUNCTION suppression_versement() RETURNS trigger AS $$
DECLARE 
    versement_effectue FLOAT;
    versement_prev FLOAT; 
    id_art INTEGER;
BEGIN      
        versement_prev := (SELECT montant FROM versement WHERE id_contrat = OLD.id_contrat AND id_versement = OLD.id_versement);   
        versement_effectue := (SELECT montant FROM paiement WHERE id_contrat = OLD.id_contrat AND id_versement = OLD.id_versement);

        IF versement_effectue >= versement_prev THEN 
            --RAISE NOTICE 'Le paiement effectué correspond bien au montant du %', OLD.id_versement || 'ème versement prévu pour le contrat ' 
            --            ||OLD.id_contrat || '. DELETE effectué avec succès. ';
            RETURN OLD;

        ELSE IF versement_effectue IS NULL THEN
            id_art := (SELECT id_artiste FROM contratAP WHERE id_contrat = OLD.id_contrat);
            RAISE NOTICE 'Impossible de supprimer le versement %', OLD.id_versement||' du contrat ' ||OLD.id_contrat || ' car le versement dûe à l''artiste '|| id_art || ' n''a pas encore été versé. ' ;  
        END IF;
        END IF;
    RETURN NULL;
END;
$$ LANGUAGE plpgsql; 

CREATE TRIGGER i_suppression_versement
BEFORE DELETE ON versement FOR EACH ROW
EXECUTE PROCEDURE suppression_versement();


/* TEST:
    TEST POUR DELETE REUSSI :
    SELECT * FROM versement WHERE id_contrat = 53;
    DELETE FROM versement WHERE id_contrat = 53 AND id_versement = 2;
    SELECT * FROM versement WHERE id_contrat = 53;

    TEST POUR ECHEC DELETE :
    SELECT * FROM versement WHERE id_contrat = 166;
    DELETE FROM versement WHERE id_contrat = 166 AND id_versement = 4;
    SELECT * FROM versement WHERE id_contrat = 166;
*/

 ----------------------------------------------------------------------------------------------------------------------------------------------

/*
    DESCRIPTION TRIGGER j_suppression_total_versement :
    Supprime TOUS les versements prévus pour un contrat si le total des sommes dûes a été versé (on retrouve la preuve
    de ces paiements dans la table paiement).
*/

DROP FUNCTION suppression_total_versement();
CREATE OR REPLACE FUNCTION suppression_total_versement() RETURNS trigger AS $$
DECLARE 

    i INTEGER;
    calcul_remu RECORD;
    montant_ver FLOAT;
    somme FLOAT;
    total FLOAT; 

 BEGIN
    somme = 0.0;
    SELECT benefice, pourcentage, remuneration INTO calcul_remu FROM contratAP WHERE id_contrat = NEW.id_contrat;
    total = calcul_remu.remuneration + (calcul_remu.benefice * calcul_remu.pourcentage) ;
   
    FOR i IN (SELECT id_versement FROM paiement WHERE id_contrat = NEW.id_contrat) LOOP
        montant_ver := (SELECT montant FROM paiement WHERE id_contrat = NEW.id_contrat AND id_versement = i LIMIT 1);
        somme = somme + montant_ver;
    END LOOP;    
    
    IF somme = total THEN 
        FOR i IN (SELECT id_versement FROM paiement WHERE id_contrat = NEW.id_contrat) LOOP
            DELETE FROM versement WHERE id_contrat = NEW.id_contrat AND id_versement = i; 
        END LOOP;
        --- RAISE NOTICE 'La totalité de la rémuneration dû a été versée et les versements concernés supprimés de la table versement. ';
    END IF ;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql; 

CREATE TRIGGER j_suppression_total_versement
AFTER INSERT ON paiement FOR EACH ROW
EXECUTE PROCEDURE suppression_total_versement();

/*
TEST TRIGGER j_suppression_total_versement
SELECT id_contrat AS id FROM paiement GROUP BY id_contrat HAVING COUNT(id_contrat) = 3 LIMIT 1;  --- on récupère un contrat id qui a 3 paiements reçus et
                                                                                                      lui en rajoute un 4ème qui fait que la somme des paiement
                                                                                                      soit égale à rémunération + pourcentage benefice 
SELECT * FROM paiement WHERE id_contrat = id ;
SELECT * FROM versement WHERE id_contrat = id ;
SELECT montant FROM paiement WHERE id_contrat = id ; 
SELECT date_prevue_vers FROM versement WHERE id_contrat = id AND id_versement = 4;
INSERT INTO paiement(id_contrat, id_versement,date_paie, montant) VALUES (id, 4,date_prevue_vers,montant);
SELECT * FROM paiement WHERE id_contrat = id ;
SELECT * FROM versement WHERE id_contrat = id ;
*/

 ----------------------------------------------------------------------------------------------------------------------------------------------

/*
DESCRIPTION TRIGGER k_supression_artiste:
    Empêche la suppression d'un artiste de la table artiste ET de la table client dont il herite si ce dernier a eu un contrat
    qui date de moins de 10 ans.
*/

DROP FUNCTION supression_artiste();
CREATE OR REPLACE FUNCTION supression_artiste() RETURNS trigger AS $$
DECLARE 
     date_f DATE; ---- date fin du dernier contrat de l'artiste 
     curr DATE;
     datef DATE;
BEGIN 
    curr := (SELECT CURRENT_DATE) ;
    date_f := (SELECT date_fin FROM contratAA WHERE id_artiste = OLD.id_artiste ORDER BY date_f DESC LIMIT 1);
    
    IF date_f IS NOT NULL THEN 
        SELECT INTO datef date_f + INTERVAL '1 year' * 10;
        IF datef  > curr THEN 
            RAISE 'Impossible de supprimer ce client car il a un contrat qui date de moins de 10 ans'; 
            RETURN NULL;
        ELSE
            RETURN OLD;
        END IF;
    END IF;    
END;
$$ LANGUAGE plpgsql; 


CREATE TRIGGER k_supression_artiste
BEFORE DELETE ON artiste FOR EACH ROW
EXECUTE PROCEDURE supression_artiste();

/*
TEST TRIGGER k_supression_artiste 

-- TEST pour un artiste qui a eu au moins un contrat ces 10 dernières années.
DELETE FROM client WHERE id_client = 5;
DELETE FROM artiste WHERE id_artiste = 5;

-- TEST pour un artiste qui n'a pas eu de contrat ces 10 dernières années.
    INSERT INTO client(nom,prenom,email,naissance) VALUES('Soukeina', 'Hadrami', 'souk@gm',DATE'2000-11-29');
    INSERT INTO artiste(id_artiste) VALUES(501);
    INSERT INTO contratAA (id_artiste ,pourcentage,date_deb,date_fin) VALUES(501, 0.3 , '2000-11-21','2001-12-29');
    DELETE FROM artiste WHERE id_artiste = 501;
*/

 ----------------------------------------------------------------------------------------------------------------------------------------------

/*
DESCRIPTION TRIGGER l_supression_agent :
    Empêche la suppression d'un agent qui a representé au moins un artiste ces 10 dernières années.
*/

DROP FUNCTION supression_agent();
CREATE OR REPLACE FUNCTION supression_agent() RETURNS trigger AS $$
DECLARE 

    date_f DATE; ---- Date fin du dernier contrat de l'artiste 
    curr DATE;
    datef DATE;

BEGIN 
    curr   := (SELECT CURRENT_DATE) ;
    date_f := (SELECT date_fin FROM represente WHERE id_agent = OLD.id_agent ORDER BY date_f DESC LIMIT 1);
    
    IF date_f IS NOT NULL THEN 
        SELECT INTO datef date_f + INTERVAL '1 year' * 10;
        IF datef  > curr THEN 
            RAISE 'Impossible de supprimer cet agent car il a representé un artiste durant ces 10 dernières années.'; 
            RETURN NULL;
        ELSE
            RETURN OLD;
        END IF;
    END IF;
    RETURN OLD;
END;
$$ LANGUAGE plpgsql; 

CREATE TRIGGER l_supression_agent
BEFORE DELETE ON agent FOR EACH ROW
EXECUTE PROCEDURE supression_agent();

/*
--- TEST TRIGGER k_supression_artiste 
    DELETE FROM agent WHERE id_agent = 5;
*/

 ----------------------------------------------------------------------------------------------------------------------------------------------

/*
DESCRIPTION TRIGGER m_supression_producteur :
    Empêche la suppression d'un producteur qui a émis au moins une demande durant ces 10 dernières années.
*/

DROP FUNCTION supression_producteur();
CREATE OR REPLACE FUNCTION supression_producteur() RETURNS trigger AS $$
DECLARE

    date_f DATE; ---- date fin du dernier contrat de l'artiste 
    curr DATE;
    datef DATE;

BEGIN 
    curr :=(SELECT CURRENT_DATE) ;
    date_f := (SELECT date_fin FROM demande WHERE id_prod = OLD.id_prod ORDER BY date_f DESC LIMIT 1);
    SELECT INTO datef date_f + INTERVAL '1 year' * 10;
    
    IF date_f IS NOT NULL THEN  
        IF datef  > curr THEN 
            RAISE 'Impossible de supprimer ce producteur car il a une demande qui date de moins de 10 ans'; 
            RETURN NULL;
        ELSE
            RETURN OLD;
        END IF;
    END IF;    
END;
$$ LANGUAGE plpgsql; 

CREATE TRIGGER m_supression_producteur
BEFORE DELETE ON producteur FOR EACH ROW
EXECUTE PROCEDURE supression_producteur();


/*
--- TEST TRIGGER m_supression_producteur
    DELETE FROM producteur WHERE id_prod = 389;
*/

 ----------------------------------------------------------------------------------------------------------------------------------------------

/* DESCRIPTION TRIGGER n_check_remu :
   Empêche la modification de l'attribut remuneration de la table contratAP à une valeur plus petite que celle fixée à la creation du contrat.
   Il empêche également la modification du pourcentage de l'artiste sur les benefices du contrat puisqu'il est également fixé à la creation
   du contrat.
*/


DROP function update_contrat();
CREATE OR REPLACE FUNCTION update_contrat() RETURNS trigger AS $$
BEGIN

    IF NEW.remuneration < OLD.remuneration THEN 
        raise 'Impossible de modifier la rémunération car celle ci est fixée au début du contrat et ne peut être modifier qu''en cas de retard de paiement.'; 
    END IF;

    IF NEW.pourcentage <> OLD.pourcentage THEN 
        raise 'impossible de modifier le pourcentage car celui ci est fixé au début du contrat.'; 
    END IF;
    RETURN NEW;
END;       
$$ LANGUAGE plpgsql; 

CREATE TRIGGER n_check_remu
BEFORE UPDATE ON contratAP FOR EACH ROW
EXECUTE PROCEDURE update_contrat();


/*
TEST TRIGGER n_check_remu :
    UPDATE contratAP SET remuneration = 10 WHERE id_artiste = 7;
    UPDATE contratAP SET pourcentage = 0.6  WHERE id_artiste = 7;
*/

 ----------------------------------------------------------------------------------------------------------------------------------------------

/* DESCRIPTION TRIGGER o_check_paiement :
   Refuse un paiement superieur au reste ((remuneration + pourcentage * benefice) - (somme des paiements dèja effectués)) dû à un artiste k.
*/

DROP function check_paiement();
CREATE OR REPLACE FUNCTION check_paiement() RETURNS trigger AS $$
DECLARE 
    total FLOAT;
    paie_rec FLOAT; 
    reste FLOAT;
    newmontant FLOAT;
    calcul_remu RECORD;
BEGIN 
    --- On calcul la somme des paiements dèja effectués :
    paie_rec := (SELECT sum(montant) FROM paiement WHERE id_contrat = NEW.id_contrat); 
   
    IF paie_rec IS NULL THEN
       paie_rec = 0.0;
    END IF;    

    --- On calcul la total dû à l'artiste sur le contrat :
    SELECT benefice, pourcentage, remuneration INTO calcul_remu FROM contratAP WHERE id_contrat = NEW.id_contrat;
    total = calcul_remu.remuneration + (calcul_remu.benefice * calcul_remu.pourcentage) ;    
    reste = total - paie_rec;
    
    reste = ROUND(reste::numeric, 2);
    newmontant = ROUND( NEW.montant::numeric, 2);
    
    IF newmontant > reste THEN 
        RAISE 'le paiement dépasse le reste du montant de la rémunération dû!';
    END IF;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql; 

CREATE TRIGGER o_check_paiement
BEFORE INSERT ON paiement FOR EACH ROW
EXECUTE PROCEDURE check_paiement();

/*
TEST :
    INSERT INTO paiement(id_versement, id_contrat, date_paie, montant) VALUES(4,166,'2022-08-05', 8000);
*/

 ----------------------------------------------------------------------------------------------------------------------------------------------

/* DESCRIPTION TRIGGER p_update_represente
    Empêche de modifier l'id de l'agent qui reprèsente un artiste ce qui assure que toutes les reprèsentations nouvelles ou passées sont conservées dans la table.
    Empêche de modifier l'id de l'artiste representé.
    Empêche la modification si le tuple qu'on essaye de modifier n'est pas la dernière représentation de l'artiste.
    Vérifie que la date_debut par laquelle on essaye de modifier la représentation ne chevauche pas toute les date précédentes(pas besoin de vérifier
    pour la date de fin car vérifié par le trigger c_represention_periode) 
*/

DROP FUNCTION update_represente();
CREATE OR REPLACE FUNCTION update_represente() RETURNS trigger AS $$
DECLARE

    max_date_fin DATE;
    tuple RECORD;
    ad_date DATE;

BEGIN  
    IF OLD.id_agent <> NEW.id_agent or OLD.id_artiste <> NEW.id_artiste THEN
        RAISE ' Impossible de modifier l''agent ou l''artiste , il faut insérer une nouvelle ligne pour ceci.';
    END IF;
    
    SELECT * INTO tuple FROM represente WHERE id_artiste = NEW.id_artiste ORDER BY date_deb DESC LIMIT 1;
    
    IF OLD.id_agent = tuple.id_agent and OLD.id_artiste = tuple.id_artiste AND OLD.date_deb=tuple.date_deb AND OLD.date_fin= tuple.date_fin THEN
    --on select la deuxième plus grande date de fin
        ad_date := (SELECT MAX(date_fin)
                    FROM represente
                    WHERE id_artiste = NEW.id_artiste AND date_fin NOT IN (SELECT MAX( date_fin )
                                                                           FROM represente
                                                                           WHERE id_artiste = NEW.id_artiste ));
       
        IF NEW.date_deb > ad_date THEN    
            RETURN NEW;
        ELSE
            RAISE NOTICE 'date_deb chevauche un contrat qui existe déjà.';
        END IF;
    ELSE
        RAISE NOTICE 'Le tuple modifié n''est pas le dernier.';    
    END IF;
    RETURN NULL;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER p_update_represente
BEFORE UPDATE ON represente
FOR EACH ROW
EXECUTE PROCEDURE update_represente();

/*  TEST TRIGGER p_update_represente

-- Essayer de modifier l'agent ou l'artiste
UPDATE represente SET id_artiste = 5 WHERE id_artiste = 4 AND id_agent = 147;

-- Essayer de modifier un tuple qui n'est pas la dernière representation de l'artiste  
UPDATE represente set date_deb = '2017-11-20' WHERE id_artiste = 4 AND id_agent = 2;

-- Modifier un tuple qui est la dernière representation de l'artiste mais qui chevauche les representation d'avant
UPDATE represente set date_deb ='2018-11-20' WHERE id_artiste=4 AND id_agent = 147;

-- Update le dernier tuple avec une bonne date_deb et/ou fin
UPDATE represente set date_deb ='2018-12-12' WHERE id_artiste=4 AND id_agent = 147;
UPDATE represente set date_fin ='2022-12-12' WHERE id_artiste=4 AND id_agent = 147;

*/

 ----------------------------------------------------------------------------------------------------------------------------------------------

/* DESCRIPTION TRIGGER q_supression_represente :
   Empêcghe la supression d'une representation qui date de moins de 10 ans;
*/
--TRIGGER supprime de represente qu'un tuple dont la période de représentation est plus grande que 10 ans 

DROP function supression_represente();
CREATE OR REPLACE FUNCTION supression_represente() RETURNS trigger AS $$
DECLARE

    datef DATE; -- date fin du dernier contrat de l'artiste 
    curr DATE;

BEGIN 
    curr := (SELECT CURRENT_DATE) ;
    SELECT INTO datef OLD.date_fin + INTERVAL '1 year' * 10;

    RAISE NOTICE ' datef%', datef;
    IF datef > curr THEN 
    RAISE NOTICE 'Impossible de supprimer cette représentation car elle date de moins de 10 ans'; 
        RETURN NULL;
    ELSE
        RETURN OLD;
    END IF;
END;
$$ LANGUAGE plpgsql; 

CREATE TRIGGER q_supression_represente
BEFORE DELETE ON represente FOR EACH ROW
EXECUTE PROCEDURE supression_represente();

/*
TEST :
    DELETE FROM represente WHERE id_artiste = 4 AND date_fin = '2017-05-15';
*/
 
 ----------------------------------------------------------------------------------------------------------------------------------------------

/* DESCRIPTION TRIGGER r_update_retenu :
   Empêche la modification de l'attribut retenu de la table proposition aprés que cellui-ci ait été set à TRUE par le trigger d_trig_audition.
*/

DROP function update_retenu();
CREATE OR REPLACE FUNCTION update_retenu() RETURNS trigger AS $$
BEGIN 
    raise notice 'Impossible de modifier retenue.';
    RETURN NULL; 
END;
$$ LANGUAGE plpgsql; 

CREATE TRIGGER r_update_retenu
BEFORE UPDATE ON proposition FOR EACH ROW
WHEN (OLD.retenu = true)
EXECUTE PROCEDURE update_retenu();

/*
TEST :
    UPDATE proposition SET retenu = FALSE WHERE id_demande = 44 AND id_artiste = 205;
*/

----------------------------------------------------------------------------------------------------------------------------------------------

/* DESCRIPTION TRIGGER r_update_retenu :
   Empêche la modification de l'attribut complet de la table demande aprés que cellui-ci ait été set à TRUE par le trigger g_choix_artiste.
*/

DROP function update_complet();
CREATE OR REPLACE FUNCTION update_complet() RETURNS trigger AS $$
BEGIN 
    raise notice 'Impossible de modifier le statut de la demande une fois qu’elle est complète.';
    RETURN NULL; 
END;
$$ LANGUAGE plpgsql; 

CREATE TRIGGER s_update_complet
BEFORE UPDATE ON demande FOR EACH ROW
WHEN (OLD.complet = TRUE)
EXECUTE PROCEDURE update_complet();

/*
TEST :
    UPDATE demande SET complet = FALSE WHERE id_demande = 163 AND id_prod = 282;
*/
