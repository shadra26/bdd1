\copy client(nom,prenom,email,naissance) FROM client.csv WITH (FORMAT csv, HEADER);
\copy artiste(id_artiste, popularite) FROM artiste.csv WITH (FORMAT csv, HEADER);
\copy producteur(id_prod) FROM producteur.csv WITH (FORMAT csv, HEADER);
\copy agent(nom, prenom) FROM agent.csv WITH (FORMAT csv, HEADER);
\copy contratAA(id_artiste,pourcentage,date_deb,date_fin,duree_det) FROM contratAA.csv WITH (FORMAT csv, HEADER);
\copy represente(id_agent , id_artiste ,date_deb,date_fin) FROM represente.csv WITH (FORMAT csv, HEADER);
\copy formation(titre) FROM formation.csv WITH (FORMAT csv, HEADER);
\copy formationArtiste(id_artiste, id_formation) FROM formationArtiste.csv WITH (FORMAT csv, HEADER);
\copy demande(id_prod, profil, projet, date_deb, date_fin) FROM demande.csv WITH (FORMAT csv, HEADER);
\copy proposition(id_demande, id_artiste) FROM proposition.csv WITH (FORMAT csv, HEADER);
\copy realisation(nom_real, recompense,type_real) FROM realisation.csv WITH (FORMAT csv);
\copy participation(id_artiste,id_real, id_formation) FROM participation.csv WITH (FORMAT csv);

-- Peuplement de la table contratAP : 
-- La table contratAP est peuplée par le trigger g_choix_artiste() que vous retrouverez dans le fichier create_triggers.sql.

--- Peuplement des tables versement et paiement :
DO $$
DECLARE
     calcul_remu RECORD;
     nb_contrat INTEGER;
     diff INTEGER;
     i INTEGER;
     
     date_deb_contrat DATE;
     date_fin_contrat DATE;
     current DATE;     
     date_vers DATE;

     montant FLOAT;
     total FLOAT; 
BEGIN
     nb_contrat := (SELECT count(id_contrat) FROM contratAP);
     current := cast( '2022-01-01' AS DATE) ;
   
     FOR i IN 1..nb_contrat LOOP  

          date_deb_contrat := (SELECT d.date_deb FROM demande d JOIN contratAP a ON d.id_demande = a.id_demande WHERE id_contrat = i);
          date_fin_contrat := (SELECT d.date_fin FROM demande d JOIN contratAP a ON d.id_demande = a.id_demande WHERE id_contrat = i);
          diff  := (date_fin_contrat - date_deb_contrat + 1)/4 ;
          
          /*   Calcul du montant total dû à l'artiste en faisant la somme de la rémuneration établie à la création du contrat + le pourcentage (calculé
               à partir la popularité de l'artiste dans le trigger g_choix_artiste) des bénéfices.
          */
          SELECT benefice, pourcentage, remuneration INTO calcul_remu FROM contratAP WHERE id_contrat = i;
          total = calcul_remu.remuneration + (calcul_remu.benefice * calcul_remu.pourcentage) ;
          montant := total/4.0;

          FOR j IN 1..4 LOOP
               SELECT INTO date_vers date_deb_contrat + INTERVAL '1 day' * (j * diff);
               INSERT INTO versement(id_versement,id_contrat,date_prevue_vers,montant)
                           VALUES(j,i,date_vers,montant);

               IF current >= date_vers THEN 
                    INSERT INTO paiement(id_contrat,id_versement,date_paie,montant)
                           VALUES(i,j,date_vers,montant);            
               END IF;
          END LOOP;
     END LOOP;
END;
$$ LANGUAGE plpgsql;



