DROP TABLE IF EXISTS client CASCADE;      
DROP TABLE IF EXISTS artiste CASCADE;         
DROP TABLE IF EXISTS producteur CASCADE;  
DROP TABLE IF EXISTS agent CASCADE;    
DROP TABLE IF EXISTS formation CASCADE;   
DROP TABLE IF EXISTS contratAP CASCADE ;    
DROP TABLE IF EXISTS demande CASCADE;    
DROP TABLE IF EXISTS proposition CASCADE;  
DROP TABLE IF EXISTS formationArtiste CASCADE;
DROP TABLE IF EXISTS represente CASCADE; 
DROP TABLE IF EXISTS participation CASCADE;
DROP TABLE IF EXISTS realisation CASCADE ;  
DROP TABLE IF EXISTS contratAA CASCADE ;  
DROP TABLE IF EXISTS versement CASCADE; 
DROP TABLE IF EXISTS paiement CASCADE ;  



CREATE TABLE client(
     id_client serial PRIMARY KEY,
     nom varchar(50),
     prenom varchar(50),
     email VARCHAR(50) UNIQUE NOT NULL ,  
     naissance DATE
);


CREATE TABLE artiste(
     id_artiste INTEGER PRIMARY KEY,
     popularite INTEGER NOT NULL DEFAULT 0
                CHECK (popularite BETWEEN 0 AND 5),
     FOREIGN KEY(id_artiste) REFERENCES client(id_client) ON DELETE CASCADE
);


CREATE TABLE producteur(
     id_prod INTEGER PRIMARY KEY,
     FOREIGN KEY(id_prod) REFERENCES client(id_client) ON DELETE CASCADE
);


CREATE TABLE agent(
     id_agent serial PRIMARY KEY,
     nom varchar(50) NOT NULL,
     prenom varchar(50) NOT NULL
);


CREATE TABLE contratAA(
     id_contrat  serial PRIMARY KEY,
     id_artiste  INTEGER UNIQUE,
     pourcentage FLOAT NOT NULL DEFAULT 0
                 CHECK (pourcentage >= 0),   
     date_deb  DATE NOT NULL,
     date_fin  DATE NOT NULL,
     duree_det BOOLEAN DEFAULT FALSE,    
     CONSTRAINT date_const CHECK (date_deb <= date_fin),
     FOREIGN KEY(id_artiste) REFERENCES artiste(id_artiste) ON DELETE CASCADE
);

CREATE TABLE represente(
     id_agent INTEGER,
     id_artiste INTEGER,
     date_deb DATE NOT NULL,
     date_fin DATE NOT NULL,
     CONSTRAINT date_const CHECK (date_deb <= date_fin),
     PRIMARY KEY (id_agent,id_artiste, date_deb, date_fin),
     FOREIGN KEY(id_agent) REFERENCES agent(id_agent)       ON DELETE CASCADE,
     FOREIGN KEY(id_artiste) REFERENCES artiste(id_artiste) ON DELETE CASCADE 
);

CREATE INDEX idx_represente
ON represente(id_artiste);

CREATE TABLE realisation(
     id_real serial PRIMARY KEY,
     nom_real   varchar(70) NOT NULL,
     recompense varchar(20) Default 'NONE'
                    CHECK (recompense IN ('PRIX A', 'PRIX B', 'PRIX C','NONE')),
     type_real varchar(20)
                    CHECK (type_real  IN ('Film', 'Piece', 'Album','Festival'))
);


CREATE TABLE formation(
     id_formation serial PRIMARY KEY,
     titre varchar(20)
                    CHECK (titre IN ('musicien', 'commedien','danseur','dcasting',
                                     'realisateur','scenariste','metteurScene')) 
);


CREATE TABLE demande(
     id_demande serial PRIMARY KEY,
     id_prod INTEGER,
     profil INTEGER,
     projet VARCHAR(50) NOT NULL,
     date_deb DATE,
     date_fin DATE,
     duree_det BOOLEAN DEFAULT TRUE,
     complet   BOOLEAN DEFAULT FALSE, 

     CONSTRAINT date_const CHECK (date_deb <= date_fin),
     FOREIGN KEY(profil) REFERENCES formation(id_formation) ON DELETE CASCADE, 
     FOREIGN KEY(id_prod) REFERENCES producteur(id_prod)    ON DELETE CASCADE   
);


CREATE TABLE contratAP(
     id_contrat serial PRIMARY KEY,
     id_demande INTEGER UNIQUE ,
     id_artiste INTEGER, 
     remuneration INTEGER,
     pourcentage FLOAT NOT NULL DEFAULT 0
               CHECK (pourcentage >= 0),   
     benefice FLOAT NOT NULL DEFAULT 0
               CHECK (benefice >= 0),
     duree_det BOOLEAN DEFAULT TRUE,
     FOREIGN KEY(id_demande) REFERENCES demande(id_demande) ON DELETE CASCADE 
);


CREATE TABLE proposition(
     id_demande INTEGER,
     id_artiste INTEGER,
     retenu BOOLEAN NOT NULL DEFAULT FALSE,
     PRIMARY KEY(id_artiste, id_demande),
     FOREIGN KEY(id_artiste) REFERENCES artiste(id_artiste)  ON DELETE CASCADE,
     FOREIGN KEY(id_demande) REFERENCES demande(id_demande)  ON DELETE CASCADE  
);


CREATE TABLE formationArtiste(
     id_artiste INTEGER, 
     id_formation INTEGER, 
     PRIMARY KEY(id_artiste, id_formation) ,
     FOREIGN KEY(id_artiste) REFERENCES artiste(id_artiste)  ON DELETE CASCADE,
     FOREIGN KEY(id_formation) REFERENCES formation(id_formation)  ON DELETE CASCADE 
);

CREATE INDEX idx_formationArtiste
ON formationArtiste(id_artiste);

CREATE TABLE participation(
     id_participation serial PRIMARY KEY,
     id_artiste INTEGER,
     id_real INTEGER,
     id_formation INTEGER,
     FOREIGN KEY(id_artiste) REFERENCES artiste(id_artiste)  ON DELETE CASCADE,
     FOREIGN KEY(id_real) REFERENCES realisation(id_real)    ON DELETE CASCADE, 
     FOREIGN KEY(id_formation) REFERENCES formation(id_formation)  ON DELETE CASCADE 
);

CREATE INDEX idx_participation
ON participation(id_artiste, id_real);

CREATE TABLE versement(
     id_versement  INTEGER,
     id_contrat INTEGER,
     date_prevue_vers DATE NOT NULL,
     montant FLOAT NOT NULL ,
     PRIMARY KEY (id_contrat,id_versement),
     FOREIGN KEY(id_contrat) REFERENCES contratAP(id_contrat) ON DELETE CASCADE 
);

CREATE INDEX idx_versement
ON versement(id_contrat);

-- Nous avons choisi d'enelever la contrainte de clef étangère sur (id_versement,id_contrat) car on veut pouvoir garder un historique des versements
-- effectués avec succès pour chaque contrat.
CREATE TABLE paiement(
     id_paie serial PRIMARY KEY,
     id_versement INTEGER,
     id_contrat INTEGER,
     date_paie DATE NOT NULL,
     montant FLOAT NOT NULL
               CHECK (montant >= 0)
     --CONSTRAINT uniq UNIQUE(id_versement, id_contrat),
     --FOREIGN KEY(id_contrat,id_versement) REFERENCES versement(id_contrat,id_versement) ON DELETE CASCADE 
);

CREATE INDEX idx_paiement
ON paiement(id_contrat, id_versement);