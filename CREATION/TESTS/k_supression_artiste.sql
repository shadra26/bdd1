\! clear
\! echo "";
\! echo "Test du TRIGGER k_supression_artiste :";
\! echo "";
\! echo "Empêche la suppression d'un artiste de la table artiste ET de la table client dont il herite si ce dernier ";
\! echo "a eu un contrat qui date de moins de 10 ans.";
\! echo "";


\! echo "TEST pour un artiste qui a eu au moins un contrat ces 10 dernières années :";
\! echo "";
\! echo "";
\! echo "DELETE FROM client WHERE id_client = 5;";
\! echo "";
\! echo "";
DELETE FROM client WHERE id_client = 5;
\! echo "";
\! echo "";
\! echo "DELETE FROM artiste WHERE id_artiste = 5;";
\! echo "";
\! echo "";
DELETE FROM artiste WHERE id_artiste = 5;
\! echo "";
\! echo "TEST pour un artiste qui n'a pas eu de contrat ces 10 dernières années :";
\! echo "";
\! echo "INSERT INTO client(nom,prenom,email,naissance) VALUES('Cecilia', 'Mamou', 'nin@gm',DATE'2000-11-29');";
INSERT INTO client(nom,prenom,email,naissance) VALUES('Cecilia', 'Mamou', 'nin@gm',DATE'2000-11-29');
\! echo "";
\! echo "INSERT INTO artiste(id_artiste) VALUES(501);";
INSERT INTO artiste(id_artiste) VALUES(502);
\! echo "";
\! echo "INSERT INTO contratAA (id_artiste ,pourcentage,date_deb,date_fin) VALUES(502, 0.3 , '2000-11-21','2001-12-29');";
INSERT INTO contratAA (id_artiste ,pourcentage,date_deb,date_fin) VALUES(502, 0.3 , '2000-11-21','2001-12-29');
\! echo "";
\! echo "DELETE FROM artiste WHERE id_artiste = 502;";
DELETE FROM artiste WHERE id_artiste = 502;