\! clear
\! echo "";
\! echo "Test du TRIGGER h_retard_paiement :";
\! echo "";
\! echo "Ce trigger est déclenché lorsqu'un paiement est effectué en retard par rapport à la date de versement prévue dans la table versement pour";
\! echo "prévue dans la table versement pour un certain contrat.Dans le cas d'un retard, le bénefice  de ce contrat baisse de 5% et la difference est ajoutée ";
\! echo "à la remunération de l'artiste.";
\! echo "";


\! echo "TEST Montat remuneration et benefice avant retard du paiement :";
\! echo "";
\! echo "SELECT remuneration, benefice from contratAP where id_contrat = 1;";
\! echo "";
SELECT remuneration, benefice FROM contratAP WHERE id_contrat = 1;
\! echo "";
\! echo "INSERT INTO paiement(id_versement, id_contrat, date_paie, montant) VALUES (4,1,'2022-02-16',78);";
\! echo "";
INSERT INTO paiement(id_versement, id_contrat, date_paie, montant) VALUES (4,1,'2022-02-16',78);
 
\! echo "";
\! echo "TEST Montat remuneration et benefice apres retard du paiement :";
\! echo "";
\! echo "SELECT remuneration, benefice from contratAP where id_contrat = 1;";
\! echo "";
SELECT remuneration, benefice FROM contratAP WHERE id_contrat = 1;
\! echo "";
\! echo " SELECT * FROM paiement WHERE id_contrat = 1 AND id_versement = 4;";
SELECT * FROM paiement WHERE id_contrat = 1 AND id_versement = 4;
\! echo "";