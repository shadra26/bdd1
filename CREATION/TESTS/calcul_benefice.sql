\! clear
\prompt 'Entrer une date de debut :' date_d
\prompt 'Entrer une date de fin :' date_f
\! echo "";
\! echo "Test de la fonction calcul_benefice(date_d DATE, date_f DATE) PL/pgSQL :"
\! echo "Calcul et renvoie le benefice total perçu par l'agence sur les contrats établis durant la période   "
\! echo "entre date_d et date_f."
\! echo "";

SELECT calcul_benefice(:date_d,:date_f);