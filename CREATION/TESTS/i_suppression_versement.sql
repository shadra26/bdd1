\! clear
\! echo "";
\! echo "Test du TRIGGER i_suppression_versement :";
\! echo "";
\! echo "Autorise la supression d'un versement prevu i pour un contrat k dans la table versement ssi le paiement";
\! echo "effectué dans la table paiement correspond bien à la somme dûe.";
\! echo "";


\! echo "TEST POUR DELETE REUSSI :";
\! echo "";
\! echo "SELECT * FROM versement WHERE id_contrat = 53;";
\! echo "";
SELECT * FROM versement WHERE id_contrat = 53;
\! echo "";
\! echo "SELECT * FROM paiement WHERE id_contrat = 53;";
\! echo "";
SELECT * FROM paiement WHERE id_contrat = 53;
\! echo "";
\! echo "DELETE FROM versement WHERE id_contrat = 53 AND id_versement = 2;";
\! echo "";
DELETE FROM versement WHERE id_contrat = 53 AND id_versement = 2; 
\! echo "";
\! echo "SELECT * FROM versement WHERE id_contrat = 53;";
\! echo "";
SELECT * FROM versement WHERE id_contrat = 53;
\! echo "";
\! echo "TEST POUR ECHEC DELETE :";
\! echo "";
\! echo "SELECT * FROM versement WHERE id_contrat = 166;";
SELECT * FROM versement WHERE id_contrat = 166;
\! echo "";
\! echo "DELETE FROM versement WHERE id_contrat = 166 AND id_versement = 4;";
DELETE FROM versement WHERE id_contrat = 166 AND id_versement = 4;
\! echo "";
\! echo "SELECT * FROM versement WHERE id_contrat = 166;";
SELECT * FROM versement WHERE id_contrat = 166;
 