\! clear
\! echo "";
\! echo "Test du TRIGGER d_trig_audition :";
\! echo "";
\! echo "Set l'attribut retenu de la table proposition à TRUE si le profil recherché pour la demande correspond ";
\! echo "à une des formations suivies par l'artiste proposé."
\! echo "";
\! echo "L'attribut retenu SET à FALSE par default pour l'artiste 28 est mis à TRUE lors de l'insertion.";
\! echo "INSERT INTO proposition(id_artiste,id_demande) VALUES (28,1);"; 
\! echo "";
INSERT INTO proposition(id_artiste,id_demande) VALUES (28,1);
\! echo "SELECT * FROM proposition WHERE id_artiste = 28 AND id_demande = 1;";
\! echo "";
SELECT * FROM proposition WHERE id_artiste = 28 AND id_demande = 1;


