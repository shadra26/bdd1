\! clear
\! echo "";
\! echo "Test du TRIGGER r_update_retenu :";
\! echo "";
\! echo "Empêche la modification de l'attribut retenu de la table proposition aprés que cellui-ci ait été set à TRUE par le trigger d_trig_audition.";

\! echo "";

\! echo "UPDATE proposition SET retenu = FALSE WHERE id_demande = 44 AND id_artiste = 205;";
UPDATE proposition SET retenu = FALSE WHERE id_demande = 44 AND id_artiste = 205;
\! echo "";
