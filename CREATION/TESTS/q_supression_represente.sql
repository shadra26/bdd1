\! clear
\! echo "";
\! echo "Test du TRIGGER q_supression_represente :";
\! echo "";
\! echo "Empêche la supression de represente d'un tuple dont la période de représentation est plus petite que 10 ans ";
\! echo "Test du TRIGGER q_supression_represente :";
\! echo "";

\! echo "";
\! echo "DELETE FROM represente WHERE id_artiste = 4 AND date_fin = '2017-05-15';";
DELETE FROM represente WHERE id_artiste = 4 AND date_fin = '2017-05-15';
\! echo "";
