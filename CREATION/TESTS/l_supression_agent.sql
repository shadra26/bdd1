\! clear
\! echo "";
\! echo "Test du TRIGGER l_supression_agent:";
\! echo "";
\! echo "Empêche la suppression d'un agent qui a representé au moins un artiste ces 10 dernières années. ";
\! echo "";


\! echo "TEST pour un agent qui a representé au moins un artiste ces 10 dernières années :";
\! echo "";
\! echo "";
\! echo " DELETE FROM agent WHERE id_agent = 5;";
\! echo "";
\! echo "";
 DELETE FROM agent WHERE id_agent = 5;
