\! clear
\! echo "";
\prompt 'Entrer une date de debut :' date_d
\prompt 'Entrer une date de fin :' date_f
\prompt 'Entrer un id_artiste :' id_art
\! echo "";
\! echo "Test de la fonction remuneration_totale(id_artiste, date_debut, date_fin) PL/pgSQL :"
\! echo "Fonction qui prend un intervalle de temps et un id d'artiste en paramètre et qui retourne le total des paiements"
\! echo "reçues par cet artiste pendant cette pariode pour tous ses contrats.";
\! echo "";
SELECT remuneration_totale(:id_art,:date_d,:date_f); 