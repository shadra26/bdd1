\! clear
\! echo "";
\! echo "Test du TRIGGER m_supression_producteur :";
\! echo "";
\! echo "Empêche la suppression d'un producteur qui a émis au moins une demande durant ces 10 dernières années. ";
\! echo "";


\! echo "TEST pour un producteur qui a émis au moins une demande ces 10 dernières années :";
\! echo "";
\! echo "";
\! echo " DELETE FROM producteur WHERE id_prod = 389;";
\! echo "";
\! echo "";
DELETE FROM producteur WHERE id_prod = 389;
