\! clear
\! echo "";
\! echo "Test du TRIGGER b_insert_represente :";
\! echo "";
\! echo "S'assure avant l'insertion d'un tuple dans la table represente que la période de representation est ";
\! echo "comprise dans la periode du contrat de l'artiste avec l'agence."
\! echo "";
\! echo "SELECT * FROM contratAA WHERE id_artiste = 19;";
SELECT * FROM contratAA WHERE id_artiste = 19;
\! echo "";
\! echo "INSERT INTO represente(id_agent,id_artiste,date_deb,date_fin) VALUES(157,19,DATE'2023-12-26',DATE'2024-07-18');"; 
\! echo "";
INSERT INTO represente(id_agent,id_artiste,date_deb,date_fin) VALUES(157,19,DATE'2023-12-26',DATE'2024-07-18');
\! echo "";


