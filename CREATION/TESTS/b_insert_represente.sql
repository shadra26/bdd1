\! clear
\! echo "";
\! echo "Test du TRIGGER b_insert_represente :";
\! echo "Empêche l'insertion d'un tuple qui assigne un agent à un artiste alors que ce dernier est dèja représenté par un autre agent à cette date";
\! echo "(empêche le chevauchement de dates de representation puisqu'un artiste ne peut être representé que par un agent au même temps)."
\! echo "";
\! echo "SELECT * FROM represente WHERE id_artiste = 1;";
\! echo "";
SELECT * FROM represente WHERE id_artiste = 1;
\! echo "";
\! echo "INSERT INTO represente(id_agent,id_artiste,date_deb,date_fin) VALUES(150,1,DATE'2019-02-02',DATE'2022-08-19');";
\! echo "";
INSERT INTO represente(id_agent,id_artiste,date_deb,date_fin) VALUES(150,1,DATE'2019-02-02',DATE'2022-08-19');
\! echo "";
\! echo "SELECT * FROM represente WHERE id_artiste = 1;"; 
\! echo "";
SELECT * FROM represente WHERE id_artiste = 1;

