\! clear
\! echo "";
\! echo "Test du TRIGGER a_insert_contratAA:"
\! echo "S'assure dès la signature d'un contrat de l'agence avec un artiste, que ce dernier est representé par un de leurs agents"
\! echo "pendant une durée d'un mois à compté de la signature du contrat en lui en affectant un."
\! echo "";

\! echo "Creation d'un nouvel artiste et ajout dans dans contratAA :";
\! echo "";
\! echo "INSERT INTO client(nom,prenom,email,naissance) VALUES('Soukeina', 'Hadrami', 'souk@gm',DATE'2000-11-29');";
\! echo "INSERT INTO artiste(id_artiste) VALUES(501);";
\! echo "INSERT INTO contratAA(id_artiste,pourcentage,date_deb,date_fin) VALUES(501,0.1,'2017-04-07','2017-07-06');"; 

INSERT INTO client(nom,prenom,email,naissance) VALUES('Soukeina', 'Hadrami', 'souk@gm',DATE'2000-11-29');
INSERT INTO artiste(id_artiste) VALUES(501);
INSERT INTO contratAA(id_artiste,pourcentage,date_deb,date_fin) VALUES(501,0.1,'2017-04-07','2017-07-06');
\! echo "";
\! echo "On retouve bien une representation d'un mois de ce nouvel artiste dans la table representation : ";
SELECT * FROM represente WHERE id_artiste = 501;

