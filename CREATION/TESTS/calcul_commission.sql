\! clear
\! echo "";
\prompt 'Entrer un id_contrat :' id_cont

\! echo "Test de la fonction SELECT calcul_commisson(id_contratAP INTEGER) PL/pgSQL :"
\! echo "Fonction qui calcul et renvoie la commission perçu par l'agence sur un contrat id_contrat passé en  "
\! echo "paramètre de la fonction."
\! echo "";

-- SELECT * FROM contratAP WHERE id_contrat = 1; -- On y voit la valeur du pourcentage
-- SELECT * FROM contratAA WHERE id_artiste = 205;  -- On y voit la valeur de la remuneration 
SELECT calcul_commisson(:id_cont); 