\! clear
\! echo "";
\! echo "Test du TRIGGER e_limit_participation :";
\! echo "";
\! echo "Empêche un artiste de participer plus d'une fois à une même réalisation avec le même rôle.";
\! echo "";
\! echo "SELECT * FROM participation WHERE id_artiste = 107;";
\! echo "";
SELECT * FROM participation WHERE id_artiste = 107;
\! echo "";
\! echo "INSERT INTO participation(id_artiste,id_real, id_formation) VALUES(107,44,3);";
\! echo "";
INSERT INTO participation(id_artiste,id_real, id_formation) VALUES(107,44,3);
\! echo "";
\! echo "SELECT * FROM participation WHERE id_artiste = 107;";
\! echo "";
SELECT * FROM participation WHERE id_artiste = 107;