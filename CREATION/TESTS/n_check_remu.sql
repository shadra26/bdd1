\! clear
\! echo "";
\! echo "Test du TRIGGER n_check_remu:";
\! echo "";
\! echo "Empêche la modification de l'attribut remuneration de la table contratAP à une valeur plus  ";
\! echo "petite que celle fixée à la creation du contrat. Il empêche également la modification du";
\! echo "pourcentage de l'artiste sur les benefices du contrat puisqu'il est également fixé à la creation";
\! echo "du contrat.";
\! echo "";

\! echo "SELECT remuneration FROM contratAP WHERE id_artiste = 7;";
SELECT remuneration FROM contratAP WHERE id_artiste = 7;
\! echo "";
\! echo "UPDATE contratAP SET remuneration = 10 WHERE id_artiste = 7;";
\! echo "";
UPDATE contratAP SET remuneration = 10 WHERE id_artiste = 7;
\! echo "";
\! echo "SELECT pourcentage FROM contratAP WHERE id_artiste = 7;";
SELECT pourcentage FROM contratAP WHERE id_artiste = 7;
\! echo "";
\! echo "UPDATE contratAP SET pourcentage = 0.6  WHERE id_artiste = 7;";
\! echo "";
UPDATE contratAP SET pourcentage = 0.6  WHERE id_artiste = 7;
\! echo "";
