\! clear
\! echo "";
\! echo "Test du TRIGGER g_choix_artiste :";
\! echo "";
\! echo "Pour chaque demande pour laquelle un artiste proposé à été retenu (attribut retenu de la table proposition a TRUE), un contratAP entre  ";
\! echo "cet artiste et le producteur est créé pour la période du projet. Il s'assure avant de créer ce contrat que l'artiste choisi est bien representé ";
\! echo "par un agent pour la période de la demande.Après la signature d'un contrat pour une certaine demande, l'attribut complet";
\! echo "de la table demande est mis à TRUE ce qui empêche d'autres artistes d'être proposés pour pour cette demande.";
\! echo "Il défini également la participation (un pourcentage) qu'a l'artiste dans les bénéfices en fonction de sa popularité."
\! echo "";

\! echo "SELECT count(*) FROM proposition WHERE retenu = true;";
SELECT count(*) FROM proposition WHERE retenu = true;
\! echo "";
\! echo "SELECT count(*) FROM contratAP;";
\! echo "";
SELECT count(*) FROM contratAP;
\! echo "SELECT count(*) FROM demande WHERE complet = true;";
\! echo "";
SELECT count(*) FROM demande WHERE complet = true;

\! echo "On voit bien sur les output ci-dessus que les propositions retenues sont bien devenues des contratAP et";
\! echo "que l'attribut 'complet' de la table demande est bien passé à TRUE. ";
\! echo "";
\! echo "On s'assure qu'un artiste ne peut plus être proposé pour une demande dèja complète :";
\! echo "";
\! echo "SELECT id_demande FROM demande WHERE complet = TRUE LIMIT 1; ";
SELECT id_demande FROM demande WHERE complet = TRUE LIMIT 1; 
\! echo "";
\prompt 'Entrer l''id_demande retournée par le query precedent :' id_dem
\! echo "INSERT INTO proposition(id_artiste,id_demande) VALUES (28,id_dem);  ";
INSERT INTO proposition(id_artiste,id_demande) VALUES (28,:id_dem); 