\! clear
\! echo "";
\! echo "Test du TRIGGER s_update_complet :";
\! echo "";
\! echo "Empêche la modification de l'attribut complet de la table demande aprés que cellui-ci ait été set à TRUE par le trigger g_choix_artiste.";
\! echo "";

\! echo "UPDATE demande SET complet = FALSE WHERE id_demande = 163 AND id_prod = 282;";
UPDATE demande SET complet = FALSE WHERE id_demande = 163 AND id_prod = 282;
\! echo "";
