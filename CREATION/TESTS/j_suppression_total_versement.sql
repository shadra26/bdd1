--\cd ..;
--\i execution.sql;

\! clear
\! echo "SELECT id_contrat AS id FROM paiement GROUP BY id_contrat HAVING COUNT(id_contrat) = 3 LIMIT 1;";
\! echo "";
SELECT id_contrat AS id FROM paiement GROUP BY id_contrat HAVING COUNT(id_contrat) = 3 LIMIT 1;
\! echo "";
\prompt 'Entrer un id_contrat :' id
\! echo "";
\! echo " SELECT * FROM paiement WHERE id_contrat = id ;";
\! echo "";
SELECT * FROM paiement WHERE id_contrat = :id ;
\! echo "";
\! echo "SELECT * FROM versement WHERE id_contrat = id ;";
\! echo "";
SELECT * FROM versement WHERE id_contrat = :id ;
\! echo "";
\! echo "SELECT montant FROM paiement WHERE id_contrat = id ;";
\! echo "";
SELECT montant FROM paiement WHERE id_contrat = :id ;
\prompt 'Entrer la date prévue du contrat :' date_p
\! echo "";
\prompt 'Entrer le montant du versement prévu à cette date :', montant
\! echo "";
\! echo "INSERT INTO paiement(id_contrat, id_versement,date_paie, montant) VALUES (id, 4,date_prevue_vers,montant);";
INSERT INTO paiement(id_contrat, id_versement,date_paie, montant) VALUES (:id, 4,:date_p,:montant);
\! echo "";
\! echo "SELECT * FROM versement WHERE id_contrat = id ;";
\! echo "";
SELECT * FROM versement WHERE id_contrat = :id ;
\! echo "";

--\cd TESTS;