/* DESCRIPTION fonction emuneration_totale():
   Fonction qui prend un intervalle de temps et un id d'artiste en paramètre et qui retourne le total des paiements
   reçues par cet artiste pendant cette pariode pour tous ses contrats.
*/

DROP FUNCTION remuneration_totale(artiste INTEGER, date_deb DATE, date_fin DATE);

CREATE OR REPLACE FUNCTION remuneration_totale(artiste INTEGER, date_deb DATE, date_fin DATE) RETURNS FLOAT AS $$
DECLARE
    total FLOAT;
BEGIN 
    IF date_deb IS NOT NULL AND date_fin IS NOT NULL THEN
        IF date_deb <= date_fin THEN

            total :=(SELECT SUM(montant) 
                    FROM paiement p JOIN contratAP c ON p.id_contrat = c.id_contrat 
                    WHERE id_artiste = artiste
                    AND date_paie BETWEEN date_deb AND date_fin);

            total = ROUND(total::numeric, 2);        
            RETURN total;        
        ELSE 
            RAISE 'La date de debut doit être inférieure à celle de fin.';                  
        END IF;
    END IF; 
        RAISE 'Les dates ne sont pas renseignées.';          
END;
$$ LANGUAGE plpgsql; 

/* TEST de la fonction remuneration_totale(artiste INTEGER, date_deb DATE, date_fin DATE)
    SELECT remuneration_totale(3,DATE'2019-10-13',DATE'2021-11-30');    
*/

 ----------------------------------------------------------------------------------------------------------------------------------------------

/* DESCRIPTION TRIGGER calcul_commisson() :
   Calcul et renvoie la commission perçu par l'agence sur un contrat id_contratAP passé en paramètre de la fonction.
*/

DROP FUNCTION calcul_commisson(id_contratAP INTEGER);
CREATE OR REPLACE FUNCTION calcul_commisson(id_contratAP INTEGER) RETURNS FLOAT AS $$
DECLARE
    commission FLOAT;
    percent FLOAT;
    AP RECORD;
BEGIN 
    SELECT id_artiste, remuneration INTO AP FROM contratAP WHERE id_contrat = id_contratAP;
    percent  := (SELECT pourcentage  FROM contratAA WHERE id_artiste = AP.id_artiste);
    commission = percent * AP.remuneration;
     
    RETURN commission;        
END;
$$ LANGUAGE plpgsql; 


/* TEST de la fonction calcul_commisson(id_contratAP INTEGER)
    SELECT * FROM contratAP WHERE id_contrat = 1; -- On y voit la valeur du pourcentage
    SELECT * FROM contratAA WHERE id_artiste = 205;  -- On y voit la valeur de la remuneration 
    SELECT calcul_commisson(1); 
*/

 ----------------------------------------------------------------------------------------------------------------------------------------------

/* DESCRIPTION TRIGGER calcul_benefice() :
   Calcul et renvoie le benefice total perçu par l'agence sur les contrats établis durant la période entre date_d et date_f
   
   .
*/

DROP FUNCTION calcul_benefice(date_d DATE, date_f DATE);
CREATE OR REPLACE FUNCTION calcul_benefice(date_d DATE, date_f DATE) RETURNS FLOAT AS $$
DECLARE
    total FLOAT;
    i INTEGER;
BEGIN 
    total = 0;
    IF date_d IS NOT NULL AND date_f IS NOT NULL THEN
        IF date_d <= date_f THEN

        FOR i IN (SELECT id_contrat FROM contratAP c JOIN demande d ON c.id_demande = d.id_demande WHERE date_fin BETWEEN date_d AND date_f) LOOP
            total = total + (SELECT calcul_commisson(i)); 
        END LOOP;
        total = ROUND(total::numeric, 2);    
        RETURN total;    
        ELSE 
            RAISE 'La date de debut doit être inférieure à celle de fin.';                  
        END IF;
    END IF; 
        RAISE 'Les dates ne sont pas renseignées.';          
END;
$$ LANGUAGE plpgsql; 


/* TEST de la fonction calcul_benefice(date_d DATE, date_f DATE)
    SELECT calcul_benefice(DATE'2019-10-13',DATE'2021-11-30');    
*/