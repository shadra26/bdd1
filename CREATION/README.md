Soukeina Hadrami 71808916, 
Cécilia Mamou 22109921.

Pour tester :

DANS LE DOSSIER CREATION :

\i execution.sql

\cd TESTS

-- Pour les triggers :

\i a_insert_contratAA.sql

\i b_insert_represente.sql

\i c_representation_periode.sql

\i d_trig_audition.sql

\i e_limit_participation.sql

\i f_popularite.sql 

\i g_choix_artiste.sql

\i h_retard_paiement.sql

\i i_suppression_versement.sql

\i j_suppression_total_versement.sql

\i k_supression_artiste.sql 

\i l_supression_agent.sql

\i m_supression_producteur.sql

\i n_check_remu.sql

\i o_check_paiement.sql 

\i p_update_represente.sql 

\i q_supression_represente.sql

\i r_update_retenu.sql

\i s_update_complet.sql

 
 -- Pour les fonctions :
 
\i remuneration_totale.sql

\i calcul_commission.sql

\i calcul_benefice.sql



